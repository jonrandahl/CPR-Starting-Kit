# CPR = Copy, Paste, Rename
## A reusable VS solution for Umbraco Projects
a standard Umbraco Solution Starting Kit - complete with Gulp Processing, Core Logic, and Unit Testing (TBC)

This is mainly a Visual Studio Solution created to expedite the initial setup for a new Umbraco project. 
The structure helps maintain the seperation of Logic from the UI and the base Umbraco instance thereby keeping everything organised and easy to find.

PR's happily accepted!
