﻿'use strict';

var basePaths = {
  src: '../cpr.starter-kit.ui/',
  dest: '../cpr.starter-kit.web/'
};

var paths = {
  media: {
    src: basePaths.src + 'assets/media/',
    dest: basePaths.dest + 'assets/media/'
  },
  scripts: {
    src: basePaths.src + 'assets/scripts/es6/',
    dest: basePaths.dest + 'assets/js/'
  },
  styles: {
    src: basePaths.src + 'assets/styles/sass/',
    dest: basePaths.dest + 'assets/css/'
  },
  files: {
    dest: basePaths.dest + 'views/'
  }
};

var appFiles = {
  styles: paths.styles.src + '**/*.scss',
  scripts: paths.scripts.src + '**/*.js',
  media: paths.media.src + '**/**',
  files: paths.files.dest + '**/*.cshtml'
};

var vendorFiles = {
  styles: '',
  scripts: ''
};

/*
	Let the magic begin
*/

var gulp = require('gulp'),
  gutil = require('gulp-util');

//Add in live reload to the watch
var browserSync = require('browser-sync').create();

//modules
var assets = require('postcss-assets'),
  autoprefixer = require('autoprefixer'),
  babel = require('gulp-babel'),
  concat = require('gulp-concat'),
  cssnano = require('gulp-cssnano'),
  deporder = require('gulp-deporder'),
  imagemin = require('gulp-imagemin'),
  mqpacker = require('css-mqpacker'),
  newer = require('gulp-newer'),
  postcss = require('gulp-postcss'),
  sass = require('gulp-sass'),
  size = require('gulp-size'),
  sourcemaps = require("gulp-sourcemaps"),
  stripdebug = require('gulp-strip-debug'),
  uglify = require('gulp-uglify');

// development mode?
var devBuild = (process.env.NODE_ENV !== 'production');

// Allows gulp --dev to be run for a more verbose output
var isProduction = true;
var sassStyle = 'compressed';
var sourceMaps = false;
var showSassErrors = false;

if (devBuild) {
  sassStyle = 'expanded';
  sourceMaps = true;
  isProduction = false;
  showSassErrors = true;
}

gulp.task('css', ['images'], function () {

  var postCssOpts = [
    assets({
      relative: basePaths.src,
      loadPaths: ['media/']
    }),
    autoprefixer(),
    mqpacker
  ];

  if (!devBuild) {
    postCssOpts.push(cssnano);
  }

  return gulp.src(vendorFiles.styles.concat(appFiles.styles))
    .pipe(sass({
      outputStyle: sassStyle,
      sourceMap: sourceMaps,
      precision: 3,
      errLogToConsole: showSassErrors
    }))
    .pipe(postcss(postCssOpts))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream());
});

gulp.task('scripts', function () {

  return gulp.src(
      [
        'node_modules/babel-polyfill/dist/polyfill.js',
        vendorFiles.scripts.concat(appFiles.scripts)
      ]
    )
    .pipe(deporder())
    .pipe(sourcemaps.init())
    .pipe(
      babel(
        {
          presets: ['es2015']
        }
      )
    )
    .pipe(concat('app.js'))
    .pipe(isProduction ? stripdebug() : gutil.noop())
    .pipe(isProduction ? uglify() : gutil.noop())
    .pipe(size())
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(browserSync.stream());
});

// Copy all static images
gulp.task('images', function () {
  return gulp.src(appFiles.media)
    .pipe(newer(paths.media.dest))
    // Pass in options to the task
    .pipe(imagemin({ optimizationLevel: 5 }))
    .pipe(gulp.dest(paths.media.dest))
    .pipe(browserSync.stream());
});


gulp.task('serve', ['css', 'scripts', 'images'], function () {
  browserSync.init({
    // Set as IIS profile, not IIS Express, if Express is 
    // needed then follow the options in the Browser Sync manual:
    // https://browsersync.io/docs/options
    proxy: {
      target: "cpr.local"
    },
    snippetOptions: {
      ignorePaths: ["umbraco/**"]
    },
  });

  gulp.watch(appFiles.styles, ['css']);
  gulp.watch(appFiles.scripts, ['scripts']);
  gulp.watch(appFiles.media, ['images']);
  gulp.watch(appFiles.files).on('change', browserSync.reload);
});

// run all tasks
gulp.task('run', ['css', 'scripts']); //remember - [css] calls [images]

// The defualt task (called when you run 'gulp')
gulp.task('default', ['serve']);
